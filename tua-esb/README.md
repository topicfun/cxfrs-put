Tua-esb Camel CDI Web Application
=================================

Prerequisites
=============

* Minimum of Java 1.8
* Maven 3.3 or greater
* WildFly application server. Refer to the WildFly Camel compatibility matrix for more information.




Building the application
------------------------

To build the application do:

    mvn clean install


Run Arquillian Tests
--------------------

By default, tests are configured to be skipped as Arquillian requires the use of a container.

If you already have a running application server, you can run integration tests with:

	 mvn clean install -P initdb,localdeploy -Drelease.version=Z.0.1
    mvn verify -Drevision=Z.0.1 -P integration-test -Darquillian.launch=JBPM-LOCAL


Or run only one CamelTestIT:

- Right click on pom.xml and maven -> mavenprofile select integration-test, after it you can run arquillian test as junit test in eclipse.

Deploying the application
-------------------------

To deploy the application to a running application server do:

    mvn clean package wildfly:deploy

The server console should display lines like the following:

    (MSC service thread 1-16) Apache Camel (CamelContext: cdi-context) is starting
    (MSC service thread 1-16) Camel context starting: cdi-context
    (MSC service thread 1-6) Bound camel naming object: java:jboss/camel/context/cdi-context
    (MSC service thread 1-16) Route: route4 started and consuming from: Endpoint[direct://start]
    (MSC service thread 1-16) Total 1 routes, of which 1 is started


Undeploying the application
---------------------------

    mvn wildfly:undeploy


CAMEL-SWAGGER
---------------------------

http://camel.apache.org/swagger-java.html