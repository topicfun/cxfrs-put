package hu.fornax.tua.esb.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import hu.fornax.tua.esb.protocol.message.JBPMMessage;

public class HeaderProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {

        JBPMMessage jbpmMessage = (JBPMMessage) exchange.getIn().getBody();

        // .setHeader("type", simple("${body[jbpmmessageHeader][type]}"))
        // .setHeader("serviceType", simple("${body[jbpmmessageHeader][serviceType]}"))

        exchange.getIn().setHeader("serviceType", jbpmMessage.getJBPMMessageHeader().getServiceType().toString());
        exchange.getIn().setHeader("type", jbpmMessage.getJBPMMessageHeader().getType().name());

    }

}
