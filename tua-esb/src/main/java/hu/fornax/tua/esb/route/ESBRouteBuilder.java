package hu.fornax.tua.esb.route;

import javax.enterprise.context.ApplicationScoped;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.model.dataformat.JsonLibrary;

import hu.fornax.tua.esb.processor.HeaderProcessor;
import hu.fornax.tua.esb.protocol.message.JBPMMessage;
import hu.fornax.tua.esb.protocol.message.JBPMServiceType;
import hu.fornax.tua.util.esb.temp.ESBLog;

/**
 * <p>
 * <b>ESBRouteBuilder osztĂˇly.</b>
 * </p>
 *
 * @author Zoltan Balogh <br>
 * 
 *         <p>
 *         <b>AttribĂştumok:</b>
 *         </p>
 *         <ul>
 *         <li></li>
 *         </ul>
 */
@ApplicationScoped
@ContextName("camel-kie-context")
public class ESBRouteBuilder extends RouteBuilder {

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.camel.builder.RouteBuilder#configure()
     */
    @Override
    public void configure() throws Exception {

        onException(Exception.class)
                .handled(true)
                .log(ESBLog.ESB_ERROR + "ESB error sent back to the client")
                // .transform(exceptionMessage())
                .bean("exceptionHandler")
                .marshal().json(JsonLibrary.Jackson)
                .log(ESBLog.ESB_ERROR + "ERROR in ESB prosessing request! ${body} ")
                .end();

        /* KIE request send to DMS */
        from("activemq:queue:KieRequest.INOUT")
                .log(ESBLog.ESB + "JBPM message in KieRequest.INOUT: body: ${body}")
                .to("direct:headerSettings")
                .unmarshal().json(JsonLibrary.Jackson, JBPMMessage.class)
                .choice()
                    .when(header("serviceType").isEqualTo(JBPMServiceType.EMAIL_SERVICE))
                        .log(ESBLog.ESB + "Email Service call")
                        .to("activemq:queue:EMAIL.INOUT.EP")
                        .log(ESBLog.ESB + "JBPM message received from Email: body: ${body} ${headers}")
                .end()
                .log(ESBLog.ESB + "Kie Service request finished")
                .marshal().json(JsonLibrary.Jackson);


        from("activemq:queue:ServiceRequest.INOUT")
                // .unmarshal().json(JsonLibrary.Jackson, JBPMMessage.class)
                .log(ESBLog.ESB + " JBPM message in ServiceRequest")
                .process(new HeaderProcessor())
                .log(ESBLog.ESB + "JBPM service type: ${header.serviceType}")
                .log(ESBLog.ESB + "JBPM message type: ${header.type}")
                .choice()
                    .when(header("serviceType").isEqualTo(JBPMServiceType.CONFIG_SERVICE))
                        .to("direct:configRoute")
                .end();

        from("direct:configRoute")
                .log(ESBLog.ESB + "Config Service call")
                // .marshal().json(JsonLibrary.Jackson)
                .to("activemq:queue:CONFIG.INOUT.EP")
                .log(ESBLog.ESB + "JBPM message received from Config Service: body: ${body} ${headers}");


        from("direct:headerSettings")
                .log(ESBLog.ESB + "Header settings: ${body}")
                // .setExchangePattern(ExchangePattern.InOut)
                .setHeader("type", simple("${body[jbpmmessageHeader][type][1]}"))
                .setHeader("serviceType", simple("${body[jbpmmessageHeader][serviceType]}"))
                .log(ESBLog.ESB + "JBPM service type: ${header.serviceType}")
                .log(ESBLog.ESB + "JBPM message type: ${header.type}");


    }
}
