package hu.fornax.tua.esb.route;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.map.SerializationConfig.Feature;

import hu.fornax.exception.ExceptionHandler;
import hu.fornax.tua.util.esb.temp.ESBLog;

/**
 * <p>
 * <b>ESBRouteBuilder osztĂˇly.</b>
 * </p>
 *
 * @author Zoltan Balogh <br>
 * 
 *         <p>
 *         <b>AttribĂştumok:</b>
 *         </p>
 *         <ul>
 *         <li></li>
 *         </ul>
 */
@ApplicationScoped
@ContextName("camel-rest-context")
public class RestRouteBuilder extends RouteBuilder {

    @Produces
    @Named("jsonProvider")
    public JacksonJaxbJsonProvider jsonProvider() {

        JacksonJaxbJsonProvider jacksonJaxbJsonProvider = new JacksonJaxbJsonProvider();
        jacksonJaxbJsonProvider.configure(Feature.FAIL_ON_EMPTY_BEANS, false);

        return jacksonJaxbJsonProvider;
    }

    @Produces
    @Named("exceptionRest")
    public ExceptionHandler exceptionProvider() {
        return new ExceptionHandler();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.camel.builder.RouteBuilder#configure()
     */
    @Override
    public void configure() throws Exception {

        // onException(Exception.class)
        // .handled(true)
        // .log(ESBLog.ESB + "na? ${body}")
        // // .bean("exceptionHandlerRest")
        // .log(ESBLog.ESB + "exception: ${exception}")
        // .setBody(simple("${exception.message}\n"))
        // .log(ESBLog.ESB + "exception body and header: ${body} ${headers}");

        from("cxfrs:http://localhost:8080/tua"
                + "?resourceClasses="
                + "hu.fornax.tua.file.protocol.rest.FileREST"
                + ",hu.fornax.tua.admin.protocol.rest.MenuREST"
                + ",hu.fornax.tua.admin.protocol.rest.MessageBoardREST"
                + ",hu.fornax.tua.kodtar.protocol.rest.KodtarREST"
                + "&loggingFeatureEnabled=true&synchronous=true"
                + "&providers=#jsonProvider")
                        .setHeader("CamelCxfRsUsingHttpAPI", constant("true"))
                        .log(ESBLog.ESB + "REST request: ${headers}")
                        .log(ESBLog.ESB + "REST path: ${header.CamelHttpUri}")
                        .choice()
                            .when(header("CamelHttpUri").contains("/tua/api/file"))
                            .setHeader("CamelCxfRsUsingHttpAPI", constant("false"))
                            .to("cxfrs:http://localhost:8080/tua-file/rest"
                                    + "?resourceClasses=hu.fornax.tua.file.protocol.rest.FileREST"
                                    + "&throwExceptionOnFailure=false&loggingFeatureEnabled=true&providers=#jsonProvider")
                            .when(header("CamelHttpUri").contains("/tua/api/menu"))
                            .to("cxfrs:http://localhost:8080/tua-menu/rest"
                                    + "?resourceClasses=hu.fornax.tua.admin.protocol.rest.MenuREST"
                                    + "&throwExceptionOnFailure=false&loggingFeatureEnabled=true&providers=#jsonProvider")
                            .when(header("CamelHttpUri").contains("/tua/api/messageboard"))
                            .to("cxfrs:http://localhost:8080/tua-messageboard/rest"
                                    + "?resourceClasses=hu.fornax.tua.admin.protocol.rest.MessageBoardREST"
                                    + "&throwExceptionOnFailure=false&synchronous=true&loggingFeatureEnabled=true&providers=#jsonProvider")
                            .when(header("CamelHttpUri").contains("/tua/api/kodtar"))
                            .to("cxfrs:http://localhost:8080/tua-kodtar/rest"
                                    + "?resourceClasses=hu.fornax.tua.kodtar.protocol.rest.KodtarREST"
                                    + "&throwExceptionOnFailure=false&synchronous=true&loggingFeatureEnabled=true&providers=#jsonProvider")
                        .otherwise()
                        .log(ESBLog.ESB + "What a call?");

    }
}
