/*
 * #%L
 * Wildfly Camel
 * %%
 * Copyright (C) 2013 - 2015 RedHat
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package hu.fornax.tua.esb.route.test;

import java.io.Closeable;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.cdi.CdiCamelExtension;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.fornax.tua.esb.protocol.iface.JBPMCommandTypeIface;
import hu.fornax.tua.esb.protocol.message.JBPMMessage;
import hu.fornax.tua.esb.protocol.message.JBPMMessageBody;
import hu.fornax.tua.esb.protocol.message.JBPMMessageHeader;
import hu.fornax.tua.esb.protocol.message.JBPMServiceMessage;
import hu.fornax.tua.esb.protocol.message.JBPMServiceType;
import hu.fornax.tua.esb.protocol.util.ObjectMapping;

/**
 * <p>
 * <b>CamelTestIT osztály.</b>
 * </p>
 *
 * @author Zoltan Balogh <br>
 * 
 *         <p>
 *         <b>Attribútumok:</b>
 *         </p>
 *         <ul>
 *         <li></li>
 *         </ul>
 */
@RunWith(Arquillian.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CamelTestIT {

    /** A/Az camel context. */
    @Inject
    protected CamelContext camelContext;

    protected ProducerTemplate producerTemplate;

    /** A/Az log. */
    protected static Logger log = LoggerFactory.getLogger(CamelTestIT.class);

    /** ESB konstans. */
    protected final static String ESB = "activemq:queue:KieRequest.INOUT";

    protected final static String SERVICE = "activemq:queue:ServiceRequest.INOUT";

    protected final static String POT = "activemq:queue:POTRequest.INOUT";

    /**
     * Létrehozza a deployment.
     * </b>
     * </p>
     * <b>Backlog címe</b>: <a href="link">...</a><br>
     * <b>Backlog leírása</b>:<br>
     * <br>
     *
     * @return WebArchive a WebArchive
     *         <br>
     *         <br>
     *         <b>Test: </b>
     * @see ...Test hivatkozás...
     */
    @Deployment
    public static WebArchive createDeployment() {

        File[] files = Maven.resolver().loadPomFromFile(".flattened-pom.xml")
                // .resolve("hu.fornax:tua-protocol:1.0.0","hu.fornax:tua-integration:pom:1.0.0")
                .importRuntimeDependencies().resolve()
                .withTransitivity().asFile();

        // File[] files = Maven.resolver().resolve("hu.fornax:tua-protocol:1.0.0_Z", "hu.fornax:tua-integration:pom:1.0.0_Z")
        // .withTransitivity().asFile();
        // File[] files = Maven.resolver()
        // .addDependencies(
        // MavenDependencies.createDependency("hu.fornax:tua-protocol:1.0.0", ScopeType.TEST, false
        // )).resolve().withTransitivity().asFile();

        final WebArchive archive = ShrinkWrap.create(WebArchive.class, "tua-esb-test.war");
        archive.addAsResource("beans.xml", "beans.xml");
        archive.addAsWebInfResource("jboss-deployment-structure.xml", "jboss-deployment-structure.xml");
        // archive.addAsResource("persistence.xml", "META-INF/persistence.xml");
        archive.addPackage(CdiCamelExtension.class.getPackage());
        archive.addAsWebInfResource("log4j.properties", "log4j.properties");

        // archive.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
        // archive.addPackage(CdiCamelExtension.class.getPackage());
        archive.addPackage(CamelTestIT.class.getPackage());
        // archive.addPackages(true, "hu.fornax.tua.protocol");

        archive.addAsLibraries(files);

        return archive;
    }

    /**
     * Létrehozza a JBPM message-t.
     *
     * @param jBPMServiceMessage
     *            A(z) jBPM mock message
     * @param jBPMServiceType
     *            A(z) jBPM mock type
     * @param jBPMCommandType
     *            A(z) jBPM command type
     * @return JBPMMessage a JBPMMessage
     * 
     * @throws Exception
     *             the exception
     */
    protected JBPMMessage createJBPMMessage(JBPMServiceMessage jBPMServiceMessage, JBPMServiceType jBPMServiceType,
            JBPMCommandTypeIface jBPMCommandType) throws Exception {

        JBPMMessageBody jbpmMessageBody = new JBPMMessageBody();
        jbpmMessageBody.setjBPMServiceMessage(ObjectMapping.ObjectToJson(jBPMServiceMessage));

        JBPMMessageHeader header = new JBPMMessageHeader();
        header.setServiceType(jBPMServiceType);
        header.setType(jBPMCommandType);

        JBPMMessage jbpmMessage = new JBPMMessage();
        jbpmMessage.setJBPMMessageHeader(header);
        jbpmMessage.setJBPMMessageBody(jbpmMessageBody);

        return jbpmMessage;
    }

    public class ProducerTemplateClosable implements Closeable {
        @Override
        public void close() {
            try {
                producerTemplate.stop();
                producerTemplate = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public ProducerTemplateClosable initProducer() {
        producerTemplate = camelContext.createProducerTemplate();
        return new ProducerTemplateClosable();
    }

    protected <T> T requestJson(JBPMServiceMessage message, JBPMServiceType serviceType,
            JBPMCommandTypeIface commandType, Class<T> responseBodyType, String endpointUri) throws Exception {
        JBPMMessage jbpmMessage = createJBPMMessage(message, serviceType, commandType);
        String result = producerTemplate.requestBody(endpointUri, ObjectMapping.ObjectToJson(jbpmMessage), String.class);
        // System.out.println("Result: " + result);
        return ObjectMapping.JsonToObject(ObjectMapping.JsonToObject(result, JBPMMessage.class)
                .getJBPMMessageBody().getjBPMServiceMessage(), responseBodyType);
    }

    protected <T> T request(JBPMServiceMessage message, JBPMServiceType serviceType,
            JBPMCommandTypeIface commandType, Class<T> responseBodyType,
            String endpointUri) throws Exception {
        JBPMMessage jbpmMessage = createJBPMMessage(message, serviceType, commandType);
        JBPMMessage result = producerTemplate.requestBody(endpointUri, jbpmMessage, JBPMMessage.class);
        // System.out.println("Result: " + result);
        return ObjectMapping.JsonToObject(result.getJBPMMessageBody().getjBPMServiceMessage(), responseBodyType);
    }

    protected <T> T kieRequest(JBPMServiceMessage message, JBPMServiceType serviceType,
            JBPMCommandTypeIface commandType, Class<T> responseBodyType) throws Exception {
        return requestJson(message, serviceType, commandType, responseBodyType, ESB);
    }

    protected <T> T serviceRequest(JBPMServiceMessage message, JBPMServiceType serviceType,
            JBPMCommandTypeIface commandType, Class<T> responseBodyType) throws Exception {
        return request(message, serviceType, commandType, responseBodyType, SERVICE);
    }

    //
    // /**
    // * TesztMetódus
    // *
    // * <p>
    // * <b>Teszt megnevezése: 2 tua 3 process start backend request</b>
    // * </p>
    // * Teszt sorszám: 0.
    // *
    // * @throws Exception
    // *
    // * @see ...tesztelt metódus hivatkozás...
    // */
    // @Ignore
    // @Test
    // public void test0202TuaProcessStartBackendRequest() throws Exception {
    //
    // ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
    // ObjectMapper mapper = new ObjectMapper();
    // String jsonJBPMMessage = null;
    // try {
    // jsonJBPMMessage = mapper.writeValueAsString(createStartProcessJbpmMessage());
    // } catch (JsonProcessingException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    //
    // log.debug("jsonJBPMMessage:" + jsonJBPMMessage);
    //
    // String result = producerTemplate.requestBody(ESB, jsonJBPMMessage,
    // String.class);
    // JBPMMessage responseObject = ObjectMapping.JsonToObject(result, JBPMMessage.class);
    // StartProcessResponseDto startProcessResponseDto = ObjectMapping
    // .JsonToObject(responseObject.getJBPMMessageBody().getjBPMServiceMessage(), TuaMessage.class)
    // .getStartProcessResponseDto();
    // System.out.println("OLIR3 Backend result: " + startProcessResponseDto.toString());
    // Assert.assertFalse(startProcessResponseDto.getStartedProcessIdList().isEmpty());
    // try {
    // producerTemplate.stop();
    // } catch (Exception e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // }

    // @Ignore
    // @Test
    // public void test0203TuaSzarmazasiAdatokRequest() throws Exception {
    //
    // ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
    // String jsonJBPMMessage = null;
    //
    // jsonJBPMMessage = ObjectMapping.ObjectToJson(createSzarmazasiAdatokJbpmMessage());
    //
    // log.debug("jsonJBPMMessage:" + jsonJBPMMessage);
    //
    // String result = producerTemplate.requestBody("activemq:queue:KieRequest.INOUT", jsonJBPMMessage,
    // String.class);
    // // JBPMMessage responseObject = ObjectMapping.JsonToObject(result, JBPMMessage.class);
    //
    // // System.out.println(responseObject);
    // System.out.println("SZARAMZASI ADATOK RESULT: " + result);
    // try {
    // producerTemplate.stop();
    // } catch (Exception e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // }

    // private JBPMMessage createSzarmazasiAdatokJbpmMessage() {
    // JBPMMessage jbpmMessage = new JBPMMessage();
    // TuaMessage tuaMessage = new TuaMessage();
    //
    // JBPMMessageHeader header = new JBPMMessageHeader();
    // JBPMMessageBody jbpmMessageBody = new JBPMMessageBody();
    // jbpmMessage.setJBPMMessageBody(jbpmMessageBody);
    // OriginDataUpdateDto originDataUpdateDto = new OriginDataUpdateDto();
    // originDataUpdateDto.setNumberInterval("XU");
    // IvarDto ivarDto = new IvarDto();
    // ivarDto.setMegnevezes("mén");
    // originDataUpdateDto.setSex(ivarDto);
    // tuaMessage.setOriginDataUpdateDto(originDataUpdateDto);
    // header.setType(JBPMCommandType.GENERATE_LOAZONOSITO);
    // header.setServiceType(JBPMServiceType.SZARMAZASI_ADATOK_KARBANTARTASA_SERVICE);
    // jbpmMessage.setJBPMMessageHeader(header);
    // try {
    // jbpmMessageBody.setjBPMServiceMessage(ObjectMapping.ObjectToJson(tuaMessage));
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    //
    // return jbpmMessage;
    // }
    // /**
    // * Létrehozza a start process jbpm message.
    // * </b>
    // * </p>
    // * <b>Backlog címe</b>: <a href="link">...</a><br>
    // * <b>Backlog leírása</b>:<br>
    // * <br>
    // *
    // * @return JBPMMessage a JBPMMessage
    // * <br>
    // * <br>
    // * <b>Test: </b>
    // * @throws Exception
    // * @see ...Test hivatkozás...
    // */
    // private static JBPMMessage createStartProcessJbpmMessage() throws Exception {
    // JBPMMessage jbpmMessage = new JBPMMessage();
    // TuaMessage tuaMessage = new TuaMessage();
    // tuaMessage.setStartProcessDto(createStartProcessDto());
    //
    // JBPMMessageHeader header = new JBPMMessageHeader();
    // JBPMMessageBody jbpmMessageBody = new JBPMMessageBody();
    // jbpmMessage.setJBPMMessageBody(jbpmMessageBody);
    // header.setServiceType(JBPMServiceType.BACKEND_SERVICE);
    // header.setType(JBPMCommandType.START_BUSINESS_PROCESS_REQUEST);
    // jbpmMessage.setJBPMMessageHeader(header);
    // jbpmMessageBody.setjBPMServiceMessage(ObjectMapping.ObjectToJson(tuaMessage));
    //
    // return jbpmMessage;
    // }

    // /**
    // * Létrehozza a start process dto.
    // * </b>
    // * </p>
    // * <b>Backlog címe</b>: <a href="link">...</a><br>
    // * <b>Backlog leírása</b>:<br>
    // * <br>
    // *
    // * @return StartProcessDto a StartProcessDto
    // * <br>
    // * <br>
    // * <b>Test: </b>
    // * @see ...Test hivatkozás...
    // */
    // private static StartProcessDto createStartProcessDto() {
    // StartProcessDto startProcessDto = new StartProcessDto();
    // startProcessDto.setBusinessCaseId(1L);
    // startProcessDto.setWayOfProcessStart(WayOfBusinessCaseStart.FROM_PROCESS.getId());
    // return startProcessDto;
    // }
    //
    
    /**
     * Egy 12 karakter hosszú számban adja vissza a dátumot.
     *
     * @return EgyediAzonosito EgyediAzonosito objektum
     */
    public String getEgyediAzonositoByDatum() {

        Date yourDate = new Date();

        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yymmddhhmmss");
        String azonosito = DATE_FORMAT.format(yourDate);

        return azonosito;
    }

    /**
     * <p>
     * <b>
     * Visszaa egy dátum Date formátumban, a String-ben megadott időnek megfelelően.
     * <br>
     * Formátum: "dd/MM/yyyy"
     * 
     * Példa: "21/12/2012"
     * 
     * @param date
     *            A(z) date
     * @return datum objektum
     * @throws ParseException
     */
    public Date getDatum(String date) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date d = sdf.parse(date);

        return d;
    }

}
