package hu.fornax.tua.esb.route.test.config;

import java.io.Closeable;
import java.util.Map;

import org.apache.camel.ProducerTemplate;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.fornax.tua.config.dto.enums.TuaConfig;
import hu.fornax.tua.config.dto.message.ConfigMessage;
import hu.fornax.tua.esb.protocol.message.JBPMCommandType;
import hu.fornax.tua.esb.protocol.message.JBPMMessage;
import hu.fornax.tua.esb.protocol.message.JBPMServiceType;
import hu.fornax.tua.esb.protocol.util.ObjectMapping;
import hu.fornax.tua.esb.route.test.CamelTestIT;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ConfigCamelTestIT extends CamelTestIT {

    public static final String SERVICE_QUEUE = "activemq:queue:ServiceRequest.INOUT";

    public static class PTC implements Closeable {
        public final ProducerTemplate producerTemplate;

        public PTC(ProducerTemplate producerTemplate) {
            this.producerTemplate = producerTemplate;
        }

        public void close() {
            try {
                producerTemplate.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public PTC getPTC() {
        return new PTC(camelContext.createProducerTemplate());
    }

    /** A/Az log. */
    protected static Logger log = LoggerFactory.getLogger(ConfigCamelTestIT.class);

    @Test
    public void test401ConfigReadDMSHost() throws Exception {
        TuaConfig config = TuaConfig.DMS_HOST;

        try (PTC ptc = getPTC()) {
            ConfigMessage configMessage = callConfigService(JBPMCommandType.CONFIG_READ_REQUEST,
                    new ConfigMessage(config), ptc.producerTemplate);
            System.out.println("Config read response: " + configMessage.toString());

            Assert.assertNotNull(configMessage.getConfigValue());
        }
    }

    @Test
    public void test402ConfigWriteDMSHost() throws Exception {
        TuaConfig config = TuaConfig.DMS_HOST;
        String tesztHost = "127.10.20.30";

        try (PTC ptc = getPTC()) {
            ConfigMessage configMessage = callConfigService(JBPMCommandType.CONFIG_WRITE_REQUEST,
                    new ConfigMessage(config, tesztHost), ptc.producerTemplate);
            System.out.println("Config write response: " + configMessage.toString());

            configMessage = callConfigService(JBPMCommandType.CONFIG_READ_REQUEST,
                    new ConfigMessage(config), ptc.producerTemplate);
            System.out.println("Config read response: " + configMessage.toString());

            Assert.assertEquals(tesztHost, configMessage.getConfigValue());
        }
    }

    @Test
    public void test403ConfigReadList() throws Exception {
        TuaConfig config1 = TuaConfig.DMS_HOST;
        TuaConfig config2 = TuaConfig.DMS_HOST;

        try (PTC ptc = getPTC()) {
            ConfigMessage configMessage = callConfigService(JBPMCommandType.CONFIG_READ_REQUEST,
                    new ConfigMessage(config1, config2), ptc.producerTemplate);
            System.out.println("Config read response: " + configMessage.toString());

            Map<String, String> values = configMessage.getConfigValues();

            Assert.assertNotNull(config1.getFrom(values));
            Assert.assertNotNull(config2.getFrom(values));
        }
    }

    private ConfigMessage callConfigService(JBPMCommandType commandType,
            ConfigMessage configMessage, ProducerTemplate producerTemplate) throws Exception {
        JBPMMessage jBPMMessage = this.createJBPMMessage(
                configMessage, JBPMServiceType.CONFIG_SERVICE, commandType);
        System.out.println("jsonJBPMMessage:" + jBPMMessage);
        JBPMMessage result = producerTemplate.requestBody(SERVICE_QUEUE, jBPMMessage,
                JBPMMessage.class);
        System.out.println("Result: " + result);
        return ObjectMapping
                .JsonToObject(result.getJBPMMessageBody().getjBPMServiceMessage(),
                        ConfigMessage.class);
    }

}
