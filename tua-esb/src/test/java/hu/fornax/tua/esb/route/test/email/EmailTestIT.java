package hu.fornax.tua.esb.route.test.email;

import java.io.Closeable;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.fornax.tua.email.dto.enums.Email;
import hu.fornax.tua.email.dto.enums.EmailCommandType;
import hu.fornax.tua.email.dto.enums.EmailParam;
import hu.fornax.tua.email.dto.message.EmailMessage;
import hu.fornax.tua.email.dto.message.EmailMessageBuilder;
import hu.fornax.tua.esb.protocol.message.JBPMServiceType;
import hu.fornax.tua.esb.route.test.CamelTestIT;

public class EmailTestIT extends CamelTestIT {
    protected static Logger log = LoggerFactory.getLogger(EmailTestIT.class);

    @Test
    public void test101SendEmail() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.");

        EmailMessageBuilder builder = new EmailMessageBuilder(Email.HATOSAGI_BIZONYITVANY_MEGKULDESE);
        builder.set(EmailParam.KERELEM_BEADAS_DATUM, sdf.format(new Date()));
        builder.set(EmailParam.LOEGYED_BELFOLDI_AZONOSITO, "teszt-lo-azon");
        builder.set(EmailParam.LOEGYED_MENFOTORZSKONYVI_SZAM, "teszt-menf-szam");
        EmailMessage emailMessage = builder.end();
        emailMessage.setToEmailAddresses(Arrays.asList("xx@gmail.com"));

        try (Closeable c = initProducer()) {
            EmailMessage emailMessage2 = kieRequest(emailMessage, JBPMServiceType.EMAIL_SERVICE,
                    EmailCommandType.EMAIL_SEND, EmailMessage.class);
            log.debug("Email send response: {}", emailMessage2);

            // Assert.assertNotNull
        }
    }
}
