package hu.fornax.tua.esb.route.test.websocket;

import java.io.Closeable;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.fornax.tua.esb.protocol.message.JBPMServiceType;
import hu.fornax.tua.esb.route.test.CamelTestIT;
import hu.fornax.tua.websocket.dto.enums.WebsocketCommandType;
import hu.fornax.tua.websocket.dto.enums.WebsocketMessageType;
import hu.fornax.tua.websocket.dto.message.WebsocketMessage;

public class WebsocketTestIT extends CamelTestIT {

    protected static Logger log = LoggerFactory.getLogger(WebsocketTestIT.class);

    @Test
    public void test100SendMessage() throws Exception {
        WebsocketMessage message = new WebsocketMessage("tesztuser", WebsocketMessageType.errorString, "Teszt hiba történt!");

        try (Closeable c = initProducer()) {
            WebsocketMessage resultMessage = kieRequest(message, JBPMServiceType.WEBSOCKET_SERVICE,
                    WebsocketCommandType.WEBSOCKET_MESSAGE_SEND_REQUEST, WebsocketMessage.class);
            log.debug("Response: {}", resultMessage);
        }
    }

}
