package hu.fornax.tua.kodtar.protocol.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import hu.fornax.tua.kodtar.dto.KodtarDto;
import hu.fornax.tua.kodtar.enums.KodtarType;
import hu.fornax.tua.util.rest.RestURI;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

@Path(RestURI.API + "/kodtar")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@Api(value = "kodtar", authorizations = { @Authorization(value = "basicAuth") })
public interface KodtarREST {

    @GET
    @Path("/{type}")
    @ApiOperation(value = "Kódtár listázás szolgáltatás.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = KodtarDto.class, responseContainer = "List", message = "Kódtár sikeresen kilistázva!") })
    public List<KodtarDto> getKodtarElemListByType(@PathParam("type") KodtarType type, @QueryParam("id") Long id);

    @POST
    @ApiOperation(value = "Kódtár elem létrehozás.")
    @ApiResponses(value = {
            @ApiResponse(code = 201, response = KodtarDto.class, message = "Kódtár elem létrehozva."),
            @ApiResponse(code = 400, message = "Hiba a kódtár elem létrehozásában.")
    })
    public Response createKodtarElem(KodtarDto dto);

    @PUT
    @Path("/{id}")
    @ApiOperation(value = "Kódtár elem módosítás.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = KodtarDto.class, message = "Kódtár elem módosítva."),
            @ApiResponse(code = 400, message = "Hiba a kódtár elem módosításában.")
    })
    public Response updateKodtarElem(@PathParam("id") Long id, KodtarDto kodtar);

    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "Kódtár elem módosítás.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = Void.class, message = "Kódtár elem törölve"),
            @ApiResponse(code = 400, message = "Hiba a kódtár elem törlésében.")
    })
    public Response deleteKodtarElem(@PathParam("id") Long id);

    @PUT
    @Path("/{id}/move")
    @ApiOperation(value = "Kódtár elem mozgatás.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = Void.class, message = "Kódtár elem mozgatás."),
            @ApiResponse(code = 400, message = "Hiba a kódtár elem mozgatás.")
    })
    public Response moveKodtarElem(@PathParam("id") Long id, @QueryParam("direction") boolean direction);
}
