package hu.fornax.tua.kodtar.protocol.service;

import java.util.List;

import hu.fornax.tua.kodtar.dto.KodtarDto;
import hu.fornax.tua.kodtar.enums.KodtarType;

public interface KodtarService {

    List<KodtarDto> getKodtarElemListByType(KodtarType type, Long id);

    KodtarDto createKodtarElem(KodtarDto dto);

    KodtarDto updateKodtarElem(Long id, KodtarDto kodtar);

    void deleteKodtarElem(Long id);

    void moveKodtarElem(Long id, boolean direction);
}
