package hu.fornax.tua.kodtar.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import hu.fornax.exception.GeneralExceptionBuilder;
import hu.fornax.tua.common.log.Logger;
import hu.fornax.tua.entity.Kodtar;
import hu.fornax.tua.kodtar.dto.KodtarDto;
import hu.fornax.tua.kodtar.enums.KodtarType;
import hu.fornax.tua.kodtar.protocol.service.KodtarService;
import hu.fornax.tua.model.facadeIFace.KodtarFacade;
import hu.fornax.tua.util.common.ValidationUtil;
import hu.fornax.tua.util.converter.ConverterIface;
import hu.fornax.tua.util.rest.exception.ParameterKeyUiCode;
import hu.fornax.tua.util.rest.exception.TuaExceptionUiCode;
import hu.fornax.tua.util.rest.exception.ValidationException;

@Stateless
public class KodtarServiceImpl implements KodtarService {

    @Inject
    private Logger logger;

    @Inject
    private KodtarFacade kodtarFacade;

    @Inject
    private ConverterIface dozerConverter;

    @Override
    public List<KodtarDto> getKodtarElemListByType(KodtarType type, Long id) {
        logger.info("Inputs - type: {}, id: {}", type, id);
        return (kodtarFacade.getKodtarElemListByType(type, id)).stream()
                .map(this::convertToKodtarDto).collect(Collectors.toList());
    }

    @Override
    public KodtarDto createKodtarElem(KodtarDto dto) {
        logger.info("Input - dto: {}", dto);
        ValidationUtil.notNullValidation("name", dto.getName());
        ValidationUtil.notNullValidation("type", dto.getType());
        Kodtar kodtar = dozerConverter.map(dto, Kodtar.class);
        logger.info("Entity: {}", kodtar);
        kodtarFacade.addEntity(kodtar);
        return convertToKodtarDto(kodtar);
    }

    @Override
    public KodtarDto updateKodtarElem(Long id, KodtarDto kodtarDto) {
        logger.info("Inputs - id: {}, kodtar: {}", id, kodtarDto);
        if (!id.equals(kodtarDto.getId())) {
            throw GeneralExceptionBuilder.create(ValidationException.class)
                    .addParameter(ParameterKeyUiCode.id1.name(), id.toString())
                    .addParameter(ParameterKeyUiCode.id2.name(), kodtarDto.getId().toString())
                    .addExceptionUiCode(TuaExceptionUiCode.ID_MISMATCH)
                    .asException();
        }
        ValidationUtil.notNullValidation("name", kodtarDto.getName());
        ValidationUtil.notNullValidation("type", kodtarDto.getType());
        Kodtar kodtar = kodtarFacade.getEntity(kodtarDto.getId());
        if (kodtarDto.getParentId() != null) {
            Kodtar parent = kodtarFacade.getEntity(kodtarDto.getParentId());
            if (parent != null) {
                logger.info("KodtarServiceImpl.updateKodtarElem(id,kodtarDto) - parent: {}", parent);
                kodtar.setParent(kodtarFacade.getEntity(kodtarDto.getParentId()));
                if (kodtarDto.getType() != parent.getType()) {
                    throw GeneralExceptionBuilder.create(ValidationException.class)
                            .addParameter(ParameterKeyUiCode.id1.name(), parent.getType().getMegnevezes())
                            .addParameter(ParameterKeyUiCode.id2.name(), kodtar.getType().getMegnevezes())
                            .addExceptionUiCode(TuaExceptionUiCode.KODTAR_TYPE_MISMATCH)
                            .asException();
                } else {
                    kodtar.setType(kodtarDto.getType());
                }
            }
        }
        kodtar.setName(kodtarDto.getName());
        return kodtarDto;
    }

    @Override
    public void deleteKodtarElem(Long id) {
        logger.info("Input - id: {}", id);
        Kodtar kodtar = kodtarFacade.getEntity(id);
        if (kodtar.getParent() == null && !(kodtar.getChildren().isEmpty())) {
            logger.info("KodtarServiceImpl.deleteKodtarElem(id) - cannot delete kodtar, because it has children. children: {}",
                    kodtar.getChildren().toString());
            throw GeneralExceptionBuilder.create(ValidationException.class)
                    .addParameter(ParameterKeyUiCode.parameter1Name.name(), "kodtar id")
                    .addParameter(ParameterKeyUiCode.parameter1Value.name(), id.toString())
                    .addExceptionUiCode(TuaExceptionUiCode.KODTAR_HAS_CHILDREN)
                    .asException();
        } else {
            logger.info("KodtarServiceImpl.deleteKodtarElem(id) - kodtar {}", kodtar);
            kodtarFacade.deleteEntity(id);
        }
    }

    /**
     * Move kodtar elem.
     * 
     * </b>
     * </p>
     * <b>Backlog címe</b>: <a href="link">...</a><br>
     * <b>Backlog leírása</b>:<br>
     * <br>
     * <br>
     * <b>Test: </b>
     *
     * @param id
     *            azonosító
     * @param direction
     *            A(z) direction
     * @see ...Test hivatkozás...
     * 
     *      Ez a függvény direction alapján felfelé(direction==true) vagy lefelé(direction==false) mozgatja a kódtár elemet a
     *      sorrendben.
     * 
     * 
     */
    @Override
    public void moveKodtarElem(Long id, boolean direction) {
        ValidationUtil.notNullValidation("direction", direction);
        logger.info("Inputs - id: {}, direction: {}", id, direction);
        Kodtar kodtar1 = kodtarFacade.getEntity(id);
        Long sorrend = kodtar1.getSorrend();
        Long parentId;
        Kodtar kodtar2;
        if (kodtar1.getParent() == null) {
            logger.debug("KodtarServiceImpl.moveKodtarElem - kodtar1.getParent() null");
            kodtar2 = kodtarFacade.moveParent(sorrend, direction, kodtar1.getType());
        } else {
            logger.debug("KodtarServiceImpl.moveKodtarElem - kodtar1.getParent() not null");
            parentId = kodtar1.getParent().getId();
            kodtar2 = kodtarFacade.moveChild(sorrend, direction, kodtar1.getType(), parentId);
        }
        if (kodtar2 != null) {
            logger.debug("KodtarServiceImpl.moveKodtarElem - kodtar2 not null");
            kodtar1.setSorrend(kodtar2.getSorrend());
            kodtar2.setSorrend(sorrend);
            logger.info("Felcserélt sorrendek - kodtar1 régi sorrend: {}, kodtar1 új sorrend: {}, kodtar2 újsorrend: {}", sorrend,
                    kodtar1.getSorrend(), kodtar2.getSorrend());
        } else {
            logger.info("Nem lehet lejebb/feljebb mozgatni az adott kódtárelemet! Kodtarelem sorrend: {}", kodtar1.getSorrend());
        }

    }

    private KodtarDto convertToKodtarDto(Kodtar kodtar) {
        KodtarDto kodtarDto = dozerConverter.map(kodtar, KodtarDto.class);
        if (kodtar.getParent() != null) {
            kodtarDto.setParentId(kodtar.getParent().getId());
        }
        return kodtarDto;
    }

}
