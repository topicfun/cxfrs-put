package hu.fornax.tua.kodtar.rest;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import hu.fornax.tua.kodtar.dto.KodtarDto;
import hu.fornax.tua.kodtar.enums.KodtarType;
import hu.fornax.tua.kodtar.protocol.rest.KodtarREST;
import hu.fornax.tua.kodtar.protocol.service.KodtarService;

@Named("kodtarRESTImpl")
public class KodtarRESTImpl implements KodtarREST {
    @Inject
    private KodtarService kodtarService;

    @Override
    public List<KodtarDto> getKodtarElemListByType(KodtarType type, Long id) {
        return kodtarService.getKodtarElemListByType(type, id);
    }

    @Override
    public Response createKodtarElem(KodtarDto dto) {
        return Response.status(Status.CREATED).entity(kodtarService.createKodtarElem(dto)).build();
    }

    @Override
    public Response updateKodtarElem(Long id, KodtarDto kodtar) {
        return Response.status(Status.OK).entity(kodtarService.updateKodtarElem(id, kodtar)).build();
    }

    @Override
    public Response deleteKodtarElem(Long id) {
        kodtarService.deleteKodtarElem(id);
        return Response.status(Status.OK).build();
    }

    @Override
    public Response moveKodtarElem(Long id, boolean direction) {
        kodtarService.moveKodtarElem(id, direction);
        return Response.status(Status.OK).build();
    }

}
