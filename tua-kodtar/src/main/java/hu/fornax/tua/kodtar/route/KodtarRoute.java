/*
 * 
 */
package hu.fornax.tua.kodtar.route;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;

import hu.fornax.exception.ExceptionHandler;
import hu.fornax.tua.kodtar.controller.HeaderProcessor;
import hu.fornax.tua.util.esb.temp.ESBLog;

/**
 * The Class AdminRoute.
 */

@ApplicationScoped
@ContextName("camel-kodtar-context")
public class KodtarRoute extends RouteBuilder {

    /**
     * Json provider.
     *
     * @return the jackson json provider
     */
    @Produces
    @Named("jsonProvider")
    public JacksonJaxbJsonProvider jsonProvider() {
        return new JacksonJaxbJsonProvider();
    }

    /**
     * Exception provider.
     *
     * @return the exception handler
     */
    @Produces
    @Named("exceptionRest")
    public ExceptionHandler exceptionProvider() {
        return new ExceptionHandler();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.camel.builder.RouteBuilder#configure()
     */
    @Override
    public void configure() throws Exception {

        from("cxfrs:http://localhost:8080/tua-kodtar/rest?resourceClasses=hu.fornax.tua.kodtar.protocol.rest.KodtarREST&loggingFeatureEnabled=true&providers=#jsonProvider,exceptionRest")
                .process(new HeaderProcessor())
                .log(ESBLog.KODTAR + "REST request: ${body} ${headers}")
                // .bean("kodtarRESTImpl", "updateKodtarElem(*,*)");
                .toD("bean:kodtarRESTImpl?method=${header.operationName}");

    }
}
